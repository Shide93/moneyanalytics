package ru.tinkoff.scala.school


import cats.effect.{ExitCode, IO, IOApp}
import com.softwaremill.sttp.{SttpBackend, SttpBackendOptions}
import com.softwaremill.sttp.asynchttpclient.cats.AsyncHttpClientCatsBackend
import ru.tinkoff.scala.school.analytics.Analytics
import ru.tinkoff.scala.school.api.Bot


object MyChartApp extends App {
  private val jan = "January"
  private val feb = "February"
  private val mar = "March"
  val categoryDatasetExample = Map(
    "продукты" -> Map(jan -> 2000, feb -> 4000, mar -> 1000),
    "вещи" -> Map(jan -> 4000, feb -> 5000, mar -> 6000),
    "проезд" -> Map(jan -> 8000, feb -> 6000, mar -> 6000),
    "развлечения" -> Map(jan -> 10600, feb -> 7000, mar -> 3000)
  )

  //  Charts.createBarChart(categoryDatasetExample)(null)
  println("Hello, world!")

}

object TelegramBotApp extends IOApp {

  val token = "1027727048:AAEgy19D7ew-uEWGOlMYGOaOXm-mcoctK_w"

  def run(args: List[String]): IO[ExitCode] = {
    val proxy: SttpBackendOptions = SttpBackendOptions.socksProxy("75.119.205.217", 44021)
    implicit val b: SttpBackend[IO, Nothing] = AsyncHttpClientCatsBackend(proxy)
    implicit val analytics: Analytics = new Analytics

    new Bot[IO](token).startPolling.map(_ => ExitCode.Success)
  }
}
