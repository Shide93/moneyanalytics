package ru.tinkoff.scala.school.api

import java.nio.charset.StandardCharsets
import java.time.LocalDate

import cats.effect.{Async, ContextShift, IO}
import cats.syntax.functor._
import cats.syntax.flatMap._
import cats.syntax.apply._
import com.bot4s.telegram.Implicits._
import com.softwaremill.sttp._
import com.bot4s.telegram.api.declarative.Commands
import com.bot4s.telegram.cats.{Polling, TelegramBot}
import com.bot4s.telegram.methods.{GetFile, SendPhoto}
import com.bot4s.telegram.models.{ChatId, File, InputFile, Message}
import com.softwaremill.sttp.{ResponseAsByteArray, SttpBackend, Uri, sttp}
import ru.tinkoff.scala.school.analytics.{Analytics, Charts, Record}
import slogging.{LogLevel, LoggerConfig, PrintLoggerFactory}


class Bot[F[_] : Async : ContextShift](val token: String)(implicit val backend: SttpBackend[F, Nothing], implicit val a: Analytics)
  extends TelegramBot(token, backend)
    with Polling[F]
    with Commands[F] {

  LoggerConfig.factory = PrintLoggerFactory()
  LoggerConfig.level = LogLevel.TRACE

  onMessage { implicit msg =>
    //    logMessage
    using(_.document) { document =>
      val r = request[File](GetFile(document.fileId))
      onFile(r)
    }
  }

  def onFile(file: F[File])(implicit msg: Message): F[Unit] = for {
    //    _ <- logMessage
    filePath <- file.fmap(f => {
      print("111" + f.filePath)
      f.filePath.get
    })
    responce <- {
      sttp.get(uri"https://api.telegram.org/file/bot$token/$filePath")
        .response(ResponseAsByteArray)
        .send()
    }
    aa <- {
      print("4444")
      makePicture(responce.unsafeBody)
    }
  } yield ()

  def makePicture(bytes: Array[Byte])(implicit msg: Message): F[Unit] = {
    reply("picture").void
    val str = new String(bytes, StandardCharsets.UTF_8)
    val records = a.parse(str)
    sendByMonthChart(msg, records)
    sendByCategoryChart(msg, records)
  }

  private def sendByMonthChart(msg: Message, records: List[Record]) = {
    val data = a.aggregateByMonth(records)
    val chart = a.createBarChart(data)
    request(SendPhoto(ChatId(msg.chat.id),
      InputFile("byMonth.png", chart))).void
  }


  private def sendByCategoryChart(msg: Message, records: List[Record]) = {
    val data = a.aggregateByCategory(records)
    val chart = a.createPieChart(data)
    request(SendPhoto(ChatId(msg.chat.id),
      InputFile("byCategory.png", chart))).void
  }

  //  def logMessage(implicit msg: Message): F[Unit] =
  //    Logger[F].info(s"Message received from user ${msg.from.fold("unknown")(_.id.toString)}")
}