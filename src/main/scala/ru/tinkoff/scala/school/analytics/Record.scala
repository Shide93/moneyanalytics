package ru.tinkoff.scala.school.analytics

import java.io.{BufferedWriter, File, FileWriter}
import java.time.LocalDate


case class Record(date: LocalDate, account: String, category: String, amount: BigDecimal, direction: Boolean) {
  override def toString: String = s"$date,$account,$category,$amount,$direction"
}


object Generate extends App {
  val r = scala.util.Random
  val categories = List("Food", "Transport", "Fun", "Home", "Kafe", "Travel", "Service", "Stuff")
  val accounts = List("Cash", "Sberbank", "Tinkoff")

  val bw = new BufferedWriter(new FileWriter(new File("test.csv")))
  for (x <- 0 to 5000) {

    val line = Record(LocalDate.now().minusDays(r.nextInt(365)),
      categories(r.nextInt(categories.length)),
      accounts(r.nextInt(accounts.length)),
      BigDecimal(r.nextInt(50000)), r.nextBoolean())
    println(line)

    bw.write(line.toString + "\n")

  }
  bw.close()
}