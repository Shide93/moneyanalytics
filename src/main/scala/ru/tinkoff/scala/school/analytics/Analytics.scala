package ru.tinkoff.scala.school.analytics

import java.time.{LocalDate, Month}

import cats.effect.IO

class Analytics {

  def parse(str: String): List[Record] = {
    val lines   = str.split("\n").toList
    val seq     = lines.map(_.split(",").toList)
    val records = seq.map(s => Record(LocalDate.parse(s(0)), s(1), s(2), BigDecimal.apply(s(3)), s(4).toBoolean))
    records
  }

  def aggregateByMonth(recs: List[Record]): Map[String, Map[String, BigDecimal]] = {
    recs
      .groupBy(r => r.category)
      .mapValues(
        lr =>
          lr.groupBy(r => r.date.getMonth.toString)
            .mapValues(lr => combineAmounts(lr))
      )
  }

  def aggregateByCategory(recs: List[Record]): Map[String, BigDecimal] = {
    recs
      .groupBy(r => r.category)
      .mapValues(lr => combineAmounts(lr))
  }

  private def combineAmounts(l: List[Record]) = {
    l.foldLeft(BigDecimal(0))((i, r) => if (r.direction) i + r.amount else i - r.amount)
  }

  def createBarChart(data: Map[String, Map[String, BigDecimal]]): Array[Byte] = {
    Charts.createBarChart(data)
  }
  def createPieChart(data: Map[String, BigDecimal]): Array[Byte] = {
    Charts.createPieChart(data)
  }
}
