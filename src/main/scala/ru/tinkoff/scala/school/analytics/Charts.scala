package ru.tinkoff.scala.school.analytics

object Charts extends scalax.chart.module.Charting {
  val defaultChartSize = (1280, 760)

  def createBarChart(data: Map[String, Map[String, BigDecimal]]): Array[Byte] = {

    val chart = LineChart(data.toCategoryDataset)
    chart.show("Spendings", defaultChartSize)

    chart.encodeAsPNG(defaultChartSize)
  }

  def createPieChart(data: Map[String, BigDecimal]): Array[Byte] = {
    val chart = PieChart.threeDimensional(data.toPieDataset)
    chart.show("By category", defaultChartSize)

    chart.encodeAsPNG(defaultChartSize)
  }
}
