name := "moneyanalytics"

version := "1.0"
scalaVersion := "2.12.1"

scalacOptions := List(
  "-encoding", "utf8",

  "-unchecked",
  "-deprecation",
  "-target:jvm-1.8")

val akkaVersion       = "2.6.0"
val akkaHttpVersion   = "10.1.10"
val circeVersion      = "0.12.1"
val circeDerVersion   = "0.12.0-M7"
val catsVersion       = "2.0.0"
val catsEffectVersion = "2.0.0"
val fs2Version        = "2.0.0"
val shapelessVersion  = "2.3.3"
val simulacrumVersion = "0.19.0"
val tofuVersion       = "0.5.1.1"
val tschemaVersion    = "0.11.5"
val derevoVersion     = "0.10.5"
val korolevVersion    = "0.14.0"
val bot4sVersion      = "4.4.0-RC2"

libraryDependencies += "com.bot4s" %% "telegram-core" % bot4sVersion
libraryDependencies += "com.bot4s" %% "telegram-akka" % bot4sVersion


libraryDependencies += "org.typelevel" %% "cats-core"        % catsVersion
libraryDependencies += "org.typelevel" %% "cats-free"        % catsVersion
libraryDependencies += "org.typelevel" %% "cats-effect"      % catsEffectVersion
libraryDependencies += "co.fs2"        %% "fs2-core"         % fs2Version
libraryDependencies += "co.fs2"        %% "fs2-io"           % fs2Version

libraryDependencies += "com.github.wookietreiber" %% "scala-chart" % "0.5.1"
libraryDependencies += "com.itextpdf" % "itextpdf" % "5.5.6"
libraryDependencies += "org.jfree" % "jfreesvg" % "3.0"

libraryDependencies += "com.typesafe.scala-logging"   %% "scala-logging"   % "3.9.2"
libraryDependencies += "ch.qos.logback"               %  "logback-classic" % "1.2.3"
libraryDependencies += "com.bot4s"                    %% "telegram-core"   % "4.3.0-RC1"
libraryDependencies += "com.softwaremill.sttp" %% "async-http-client-backend-cats" % "1.7.2"
libraryDependencies += "org.pure4s" %% "logger4s-cats" % "0.3.1"