# MoneyAnalytics
Приложение на основе телеграм-бота для анализа расходов.

### Использование:
- В бот посылается csv файл со списком операций
- Производится набор агрегаций по различным параметрам(категории, по месяцам)
- Возвращается отчет в виде текста и картинок с графиками

###### Агрегации:
- Текущий баланс: доход - расход
- По каждой категории должен суммироваться доход и расход
- Динамика расходов за период (неделя, месяц) 
- и т. п.

Предполагается строить графики с помощью https://github.com/wookietreiber/scala-chart